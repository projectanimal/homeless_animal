
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet"
    href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="resources/assets/css/style2.css">

        <style>
            h3 {text-align:center;}
            p {font-size:15px;
                text-align:center;}
            div.container {
                position:relative;
                
            }
            body
                {
                background-image:url('resources/assets/img/dog5.jpg');
                background-repeat:repeat-x;
                }
           .listview{
				margin-top:55px;
					}
        </style>

    </head>

    <body>
    <%@ include file="/WEB-INF/views/navbar.jsp" %>
 <%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %> 
<!--     <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/homelessanimal">Feed me</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="/homelessanimal/login">Login</a></li>
            <li><a href="/homlessanimal/signup">Register</a></li>
          </ul>
        </div>
      </div>
    </nav> -->
    
<div class = "listview">
    
        <div class="container">  
    <form id="contact" action="" method="post">
    <h3>Animal Created</h3>

    <fieldset>

      <p><b>Name:</b> ${model.name}</p>
    </fieldset>
    <fieldset>

      <p><b>Type:</b> ${model.type}</p>
    </fieldset>
    <fieldset>
      <p><b>Latitude:</b> ${model.lat}</p>

    </fieldset>
    <fieldset>
      <p><b>Longitude:</b> ${model.lon}</p>
    </fieldset>
    <fieldset>
      <p><b>Comment:</b> ${model.comment}</p>
    </fieldset>
    <fieldset>
      <img width="200" height="200" src='data:image/jpeg;base64, ${model.photo}' />
    </fieldset>
    
    <p class="copyright">Back To <a href="home" target="_blank" title="Colorlib">Home</a></p>
  </form>
</div>
</div>
    </body>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</html>