<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ListView</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.3.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<script src="http://maps.googleapis.com/maps/api/js?libraries=geometry&sensor=false"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<style>
.listview{
	margin-top:55px;
}

</style>
</head>
<body>
 <%@ include file="/WEB-INF/views/navbar.jsp" %>
 <%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
<!-- <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/homelessanimal">Feed me</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="/homelessanimal/login">Login</a></li>
            <li><a href="/homlessanimal/signup">Register</a></li>
          </ul>
        </div>
      </div>
    </nav> -->


<div class = "listview">
<table class="table table-bordered" id="list">
	<thead>
		
			<tr>
				<th>image</th>
				<th class="sorting">Name</th>
				<th class="sorting">Type</th>
				<th class="sorting">Distance(km)</th>
				<th>Comment</th>
				<th>Operation</th>
			</tr>
		</thead>
		<tbody>
	<c:forEach var="animal" items="${model.animals}">
		<tr>
				<td>
					<img width="50" height="50" src='data:image/jpeg;base64, ${model.photos.get(animal.id)}' />
				</td>
				<td>${animal.name}</td>
				<td>${animal.type}</td>
				<td id="distance_${animal.id}"></td>
				<td>${animal.comment}</td>
				<td><a class="btn btn-primary" href="viewinmap?lat=${animal.latitude}&lon=${animal.longitude}" role="button">View in Map</a></td>
		</tr>
	</c:forEach>
	</tbody>

</table>

</div>
<script>
$(document).ready(function() { 
					var curLat = ${model.userLat};
					var curLng = ${model.userLon};
					var dist;
					<c:forEach var="animal" items="${model.animals}">
						var corUser = new google.maps.LatLng(curLat, curLng);
						var corPet = new google.maps.LatLng(${animal.latitude}, ${animal.longitude});
						dist = google.maps.geometry.spherical.computeDistanceBetween(corUser,corPet);
						console.log("distance:"+dist);
						//c = Math.sqrt(Math.pow((curLat - ${animal.latitude}), 2)+Math.pow((curLng - ${animal.longitude}), 2));
						$('#distance_${animal.id}').html(dist/1000);
					</c:forEach>
					$('#list').DataTable({
						"columns": [{
				            "orderable": false
				        }, {
				            "orderable": true
				        }, {
				            "orderable": true
				        }, {
				            "orderable": true
				        }, {
				            "orderable": false
				        }, {
				            "orderable": false
				        },],
					});
});
</script>

</body>
</html>