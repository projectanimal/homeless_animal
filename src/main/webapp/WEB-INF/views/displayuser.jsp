<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<style>
		body{
			padding: 10px;
		}
	</style>
</head>
<body>
	<h1>User Created</h1> 
	<p><b>Name:</b> ${model.name}</p>
	<p><b>Email:</b> ${model.email}</p>
	<p><b>Password:</b> ${model.password}</p>
	
	
 	
 	<br><a href="home" class="btn btn-success">Back To Home</a>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</html>