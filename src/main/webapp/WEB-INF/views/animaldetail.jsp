<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">


<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style>
body {
	padding: 10px;
	 background-image:url('resources/assets/img/cat1.jpg');
                background-repeat:repeat-x;
}
.listview{
	margin-top:55px;
}
</style>
</head>

<body>

 <%@ include file="/WEB-INF/views/navbar.jsp" %>
 <%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
	<div class = "listview">
	<div>
		<div class="container col-xs-12 col-md-3" >
			<h2>Profile</h2>
			<div class="panel panel-primary">
				<div class="panel-heading">Animal Info</div>
				<div class="panel-body">
					<img width="200" height="200"
						src='data:image/jpeg;base64, ${model.photo}' /> <br> <br>
					<p>
						<b>Name:</b> ${model.name}
					</p>
					<p>
						<b>Type:</b> ${model.type}
					</p>
					<p>
						<b>Sex:</b> ${model.sex}
					</p>
					<p>
						<b>Description:</b> ${model.comment}
					</p>

				</div>
				<div class="panel-footer"></div>
			</div>
		</div>

		<div class="container col-xs-12 col-md-9">
			<h2>Feeding History</h2>
			<div class="panel panel-primary">
				<div class="panel-heading">Feed History</div>
				<div class="panel-body">
					<div class="table-responsive" style="height: 362px; overflow: auto">
						<table class="table table-striped" id="list">
							<thead>
								<tr>
									<th class="sorting">User</th>
									<th class="sorting">Date</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="feedhistory" items="${model.feedhistorys}">
									<tr>
										<td>${feedhistory.user.id}</td>
										<td>${feedhistory.date}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div style="margin:16px">
	
		<a href="home" class="btn btn-success" id="addfeedinghistory" style="float:left">Feed</a>
		<a href="home" class="btn btn-success" style="float:right">Back To
			Home</a>
	</div>
	</div>
</body>
<script>
	$("#addfeedinghistory").click(function() {
		$.post("animaldetail", {
			animalid : "${model.animalid}"
		}, function() {
			window.location.href = 'home'
		});
	});
</script>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</html>