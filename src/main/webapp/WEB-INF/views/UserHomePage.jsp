<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">


<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>



<style>
			body
                {
                background-image:url('resources/assets/img/cat1.jpg');
                background-repeat:repeat-x;
                }
           .listview{
				margin-top:55px;
					}
</style>
</head>

<body>
 <%@ include file="/WEB-INF/views/navbar.jsp" %>
 <%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
<!--     <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/homelessanimal">Feed me</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="/homelessanimal/login">Login</a></li>
            <li><a href="/homlessanimal/signup">Register</a></li>
          </ul>
        </div>
      </div>
    </nav> -->
    
<div class = "listview">

	<div class="container col-xs-12 col-md-3">
		<h2>Profile</h2>
		<div class="panel panel-primary">
			<div class="panel-heading">My Info</div>
			<div class="panel-body">
				<img alt="profile" src="resources/assets/img/head.jpg">
				<p>
					<b>Name:</b> ${model.id}
				</p>
				

				<p>
					<b>Email:</b> ${model.email}
				</p>
				<form action="modifyuserinfo" method="get">
					<div class="form-group">
						<input type="hidden" name="id" value="${model.id}" /> <input
							type="submit" class="form-control btn btn-success" value="Edit" />
					</div>

				</form>


			</div>
			<div class="panel-footer">Panel Footer</div>
		</div>
	</div>

	<div class="container col-xs-12 col-md-9">
		<h2>Record</h2>
		<div class="panel panel-primary">
			<div class="panel-heading">Feed History</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped" id="list">
						<thead>
							<tr>
								<!--  	<th class="sorting">FeedId</th>
								<th class="sorting">User</th>-->
								<th class="sorting">Animal</th>
								<th class="sorting">Date</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="feedhistory" items="${model.feedhistorys}">
								<tr>
									
									<td>${feedhistory.animal.name}</td>
									<td>${feedhistory.date}</td>
								</tr>
							</c:forEach>
						</tbody>

					</table>
				</div>
			</div>
			<div class="panel-footer">Total:${model.totalrecord}</div>
		</div>
	</div>

</div>
</body>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</html>