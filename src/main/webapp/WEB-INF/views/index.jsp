<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Home</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.3.js"></script>
<script
	src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>
<style>
.mapholder {
	margin-top: 10px;
}

.viewinlist {
	margin-top: 50px;
}
</style>
</head>
<body>
	<!-- 
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/homelessanimal">Feed me</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
          	<li><a onClick="goProfile()">Profile</a></li>
            <li><a href="/homelessanimal/login">Login</a></li>
            <li><a href="/homelessanimal/signup">Register</a></li>
          </ul>
        </div>
      </div>
    </nav>
-->
	<%@ include file="/WEB-INF/views/navbar.jsp"%>
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp"%>

	<div id="viewInList" class="viewinlist">
		<button class="btn btn-primary" type="button" onclick="goList()">View
			in List</button>
	</div>
	<div id="mapview">
		<p id="demo"></p>



		<div id="mapholder" class="mapholder"></div>
	</div>
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4x-LidxFDRC97OP7CDnpUjytHBhFA79E"></script>

	<script>
var x = document.getElementById("demo");
function getLocation() {
	<c:if test="${empty model.lat}">
		if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(showPosition, showError);
	    } else {
	        x.innerHTML = "Geolocation is not supported by this browser.";
	    }
	</c:if>
	
	<c:if test="${not empty model.lat}">
		var position = {};
		position.coords = {};
		position.coords.latitude = ${model.lat};
		position.coords.longitude = ${model.lon};
		showPosition(position);
	</c:if>
	
}

function goList(){
	navigator.geolocation.getCurrentPosition(success, error);
}

function goProfile(){
	
	window.location.href = '/homelessanimal/userhomepage?id='+"${model.id}";
}


function success(pos) {
	  var crd = pos.coords;
	  var lat = crd.latitude;
	  var lon = crd.longitude;
	  window.location.href = '/homelessanimal/listview?lat='+lat+'&lon='+lon;
	};

	function error(err) {
	  console.warn('ERROR(' + err.code + '): ' + err.message);
	};

google.maps.event.addDomListener(window, 'load', getLocation);

function showPosition(position) {
	lat = position.coords.latitude;
    lon = position.coords.longitude;
    latlon = new google.maps.LatLng(lat, lon)
    mapholder = document.getElementById('mapholder')
    mapholder.style.height = '500px';
    mapholder.style.width = '100%';
    

    var myOptions = {
    center:latlon,zoom:14,
    mapTypeId:google.maps.MapTypeId.ROADMAP,
    mapTypeControl:false,
    navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
    }
    
    var map = new google.maps.Map(document.getElementById("mapholder"), myOptions);
    var marker = new google.maps.Marker({
		position:latlon,
		map:map,
		title:"I am here!",
		});
    <c:forEach var="animal" items="${model.animals}">
    	var petLat = ${animal.latitude};
    	var petLon = ${animal.longitude};
    	var petLatLon = new google.maps.LatLng(petLat, petLon);
/*     	var image = new Image();
    	image.url = 'data:image/png;base64,${model.photos.get(animal.id)}' 
    	image.scaledSize = new google.maps.Size(50,50); */
    	var image = {
    		    url: 'data:image/png;base64,${model.photos.get(animal.id)}', // url
    		    scaledSize: new google.maps.Size(50, 50), // scaled size
    		    origin: new google.maps.Point(0,0), // origin
    		    anchor: new google.maps.Point(0, 0) // anchor
    		};
    	var marker = new google.maps.Marker({
    		position:petLatLon,
    		map:map,
    		title:"animal is here!",
    		icon: image,
    		});
    	marker.setAnimation(google.maps.Animation.BOUNCE);
    	marker.addListener('click', function() {
    		window.location.href = '/homelessanimal/animaldetail?animalid='+${animal.id};
    	  });
    	
    	
    </c:forEach>
    
      google.maps.event.addListener(map, "rightclick", function(event) {
    		    var lat = event.latLng.lat();
    		    var lng = event.latLng.lng();
    		    // populate yor box/field with lat, lng
    		    //alert("Lat=" + lat + "; Lng=" + lng);
    		    window.location.href = '/homelessanimal/createanimal?lat='+lat+'&lon='+lng;
    		});
    
}

function showError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            x.innerHTML = "User denied the request for Geolocation."
            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML = "Location information is unavailable."
            break;
        case error.TIMEOUT:
            x.innerHTML = "The request to get user location timed out."
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML = "An unknown error occurred."
            break;
    }
}

</script>

</body>
</html>