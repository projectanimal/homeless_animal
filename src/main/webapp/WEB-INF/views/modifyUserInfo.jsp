<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title>Modify User Information</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    
    <!-- Added -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="resources/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="resources/assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="resources/assets/css/form-elements.css">
        <link rel="stylesheet" href="resources/assets/css/style.css">
        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="resources/assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="resources/assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="resources/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="resources/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="resources/assets/ico/apple-touch-icon-57-precomposed.png">
        
    <script src="http://static.runoob.com/assets/jquery-validation-1.14.0/lib/jquery.js"></script>
    <script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
    <script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_en.js"></script>
    
    <script>
        $.validator.setDefaults({
            submitHandler: function() {
              alert("提交事件!");
            }
        });
        $().ready(function() {
            $("#commentForm").validate();
        });
    </script>

    
    <style>
        body{
            padding: 10px;
            background-image:url('images/bg2.jpg');

        }
        .error{
         color:red;
        }
        .listview{
		margin-top:55px;
		}
        
    </style>
    <!-- Javascript -->
        <script src="resources/assets/js/jquery-1.11.1.min.js"></script>
        <script src="resources/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="resources/assets/js/jquery.backstretch.min.js"></script>
        <script src="resources/assets/js/scripts.js"></script>
        
</head>

<body>
 <%@ include file="/WEB-INF/views/navbar.jsp" %>
 <%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
<!-- <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/homelessanimal">Feed me</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="/homelessanimal/login">Login</a></li>
            <li><a href="/homlessanimal/signup">Register</a></li>
          </ul>
        </div>
      </div>
    </nav> -->
    
<div class = "listview">

        <!-- Top content -->
        <div class="top-content">
            
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>User</strong> Info</h1>

                            <!-- <p class="error">${model.loginError}</p> -->
                            
                            <div class="description">
                                <p>
                                    You can modify details of account.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                            <div class="form-top">
                                <div class="form-top-left">
                                    <h3>Edit Profile</h3>
                                    <p>Edit your name, email and password:</p>
                                </div>
                                <div class="form-top-right">
                                    <i class="fa fa-lock"></i>
                                </div>
                            </div>
                            <div class="form-bottom">

                                

                                <form role="form" action="modifyuserinfo" method="post" class="login-form" autocomplete="off">
                                    <div class="form-group">
                                        <input type="hidden" class="form-username" name="id" value="${model.id}" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="form-username">UserName</label>
                                        <input type="text" name="name" placeholder="Enter Name..." class="form-username form-control" id="form-username" readonly onfocus="$(this).removeAttr('readonly');" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="form-username">Email</label>
                                        <input type="text" name="email" placeholder="Enter Email..." class="form-username form-control" id="form-username" required/>
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="form-password">Password</label>
                                        <input type="password" name="password" placeholder="Enter Password..." class="form-password form-control" id="form-password" required/>
                                    </div>
                                    <button type="submit" class="btn">Commit</button>
                                </form>
                                <form action="userhomepage" method="get">
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" name="id" value="${model.id}">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn">Return</button>
                                       <!--  <input type="submit" class="btn" value="Return"> -->
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
</div>
    <script>
<!--    $(document).ready(function(){

        $("input[name=name]").focus();


    -->
    </script>
    
</body>

        
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</html>