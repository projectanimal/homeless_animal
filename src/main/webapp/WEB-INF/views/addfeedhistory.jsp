<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<style>
		body{
			padding: 10px;
		}
	</style>
</head>
<body>
 <%@ include file="/WEB-INF/views/navbar.jsp" %>
 <%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
<!--    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/homelessanimal">Feed me</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
          	<li><a onClick="goProfile()">Profile</a></li>
            <li><a href="/homelessanimal/login">Login</a></li>
            <li><a href="/homelessanimal/signup">Register</a></li>
          </ul>
        </div>
      </div>
    </nav> -->
	<h1>Registration</h1> 
	
	<form class="row" action="addfeedhistory" method="post" >
		
		
		<div class="form-group col-xs-12">
		  <label>UserId:</label>
		    <input type="text" class="form-control" name="userid">
		</div>
		<div class="form-group col-xs-12">
		    <label>AnimalId:</label>
		    <input type="text" class="form-control" name="animalid">
		</div>
		
		<div class="form-group col-xs-12">
			<input type="submit" class="btn btn-success"/>
		</div>
	</form>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</html>