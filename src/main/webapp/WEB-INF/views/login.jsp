<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Feedme Login</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	    
	    <!-- Added -->
		

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="resources/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="resources/assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="resources/assets/css/form-elements.css">
        <link rel="stylesheet" href="resources/assets/css/style.css">
        
        
	<style>
		body{
			padding: 10px;
		}
		.error{
			color:red;
		}
	</style>
	
	<!-- Javascript -->
        
</head>

<body>
		<script>
			
			 window.fbAsyncInit = function() {
				
				 // initialize facebook javascript SDK environment
			    FB.init({
			      appId      : '1776644119251410',
			      xfbml      : true,
			      version    : 'v2.8'
			    });
				 
				 
			   	// check login status every 1 second
			   	// if connected then redirect to home page through loginwithfacebook controller
			    setInterval(function(){ 
			    	FB.getLoginStatus(function(response) {
			    	  if (response.status === 'connected') {
				    		FB.api('/me', function(meResponse) {
				    			window.location = "loginwithfacebook?token="+response.authResponse.accessToken +
				    					"&id=" + response.authResponse.userID +
				    					"&name=" + meResponse.name;
			    		});
			    	  }
			    	});	
			    }, 1000);
			    
			    FB.AppEvents.logPageView();
			  };
			  
			  // import facebook javascript SDK
			  (function(d, s, id){
			     var js, fjs = d.getElementsByTagName(s)[0];
			     if (d.getElementById(id)) {return;}
			     js = d.createElement(s); js.id = id;
			     js.src = "//connect.facebook.net/en_US/sdk.js";
			     fjs.parentNode.insertBefore(js, fjs);
			   }(document, 'script', 'facebook-jssdk'));
		</script>
        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>Feedme</strong> Login</h1>

                            <%-- <p class="error">${model.loginError}</p> --%>
                            
                            <div class="description">
                            	<p>
	                            	Homeless Animal Feeding Web Application. Without Account?<a href="<c:url value="signup.htm"/>"><strong>To Register</strong></a>
                            	</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Login to Feedme</h3>
                            		<p>Enter your user name and password to log in:</p>
                            		<p class="error">${model.loginError}</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-lock"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">

                                <p class="error">${model.loginError}</p>

			                    <form role="form" action="login" method="post" class="login-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">User Name</label>
			                        	<input type="text" name="id" placeholder="UserName..." class="form-username form-control" id="form-username">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Password</label>
			                        	<input type="password" name="password" placeholder="Password..." class="form-password form-control" id="form-password">
			                        </div>
			                        <button type="submit" class="btn">Login</button>
			                    </form>

		                    </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 social-login">
                        	<div class="social-login-buttons">
	                        	<div class="fb-login-button" data-max-rows="1" data-size="medium" data-show-faces="false" data-auto-logout-link="false"></div>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </body>

        
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->


        <script src="resources/assets/js/jquery-1.11.1.min.js"></script>
        <script src="resources/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="resources/assets/js/jquery.backstretch.min.js"></script>
        <script src="resources/assets/js/scripts.js"></script>
</html>