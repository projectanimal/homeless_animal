<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	    
	    <!-- Added -->
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Feedme Create Animal</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="resources/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="resources/assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="resources/assets/css/form-elements.css">
        <link rel="stylesheet" href="resources/assets/css/style.css">
        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="resources/assets1/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="resources/assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="resources/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="resources/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="resources/assets/ico/apple-touch-icon-57-precomposed.png">
        
	
	
	<!-- Javascript -->
        <script src="resources/assets/js/jquery-1.11.1.min.js"></script>
        <script src="resources/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="resources/assets/js/jquery.backstretch.min.js"></script>
        <script src="resources/assets/js/scripts.js"></script>

        <style>
        body{
        	background-image:url('images/1.jpg');
            padding: 10px;
        }
        .error{
            color:red;
        }
        .listview{
			margin-top:55px;
		}
    </style>
        
</head>
    
<body>
 <%@ include file="/WEB-INF/views/navbar.jsp" %>
 <%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
<!-- <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/homelessanimal">Feed me</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="/homelessanimal/login">Login</a></li>
            <li><a href="/homlessanimal/signup">Register</a></li>
          </ul>
        </div>
      </div>
    </nav> -->

<div class = "listview">

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>Register</strong>An Animal</h1>

                            
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Create a animal account</h3>
                            		<p>Enter animal's position, name and description, and choose its sex to reigister:</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-lock"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">

                                <p class="error">${model.loginError}</p>

			                    <form role="form" action="createanimal" method="post" class="login-form" enctype="multipart/form-data">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Latitude</label>
			                        	<input type="text" name="lat" placeholder="Latitude..." class="form-username form-control" id="form-username" value="${model.lat}" readonly>
			                        </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="form-username">Longitude</label>
                                        <input type="text" name="lon" placeholder="Longitude..." class="form-username form-control" id="form-username" value="${model.lon}" readonly>
                                    </div>
			                        <div class="form-group">
                                        <label class="sr-only" for="form-username">Name</label>
                                        <input type="text" name="name" placeholder="Animal name..." class="form-username form-control" id="form-username">
                                    </div>
                                    <div class="form-group">
                                        <label>Sex:</label>
                                        <div class="radio">
                                            <label><input type="radio" name="sex" checked value="female">Female</label>&nbsp;&nbsp;&nbsp;
                                            <label><input type="radio" name="sex" value="male">Male</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Type:</label>
                                        <div class="radio">
                                            <label><input type="radio" name="type" checked value="Cat">Cat</label>&nbsp;&nbsp;&nbsp;
                                            <label><input type="radio" name="type" value="Dog">Dog</label>&nbsp;&nbsp;&nbsp;
                                            <label><input type="radio" name="type" value="Others">Others</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="form-username">Description</label>
                                        <textarea type="text" name="comment" placeholder="Comment..." class="form-username form-control" id="form-username"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Upload photo:</label> <input type="file" name="photo"
                                        accept="image/*" required />
                                    </div>


			                        <button type="submit" class="btn">Submit</button>
			                    </form>

		                    </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
       </div>
    </body>

        
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</html>