package com.elec5619.homelessanimal.service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.homelessanimal.domain.Animal;

@Service(value="animalManager")
@Transactional
public class AnimalManager implements IAnimalManager{
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	@Override
	public void addAnimal(Animal animal) {
		this.sessionFactory.getCurrentSession().save(animal);
	}
	
	@Override
	public Animal getAnimalById(long id) {
		// haven't tested
		Animal animal = (Animal)this.sessionFactory.getCurrentSession().get(Animal.class, id);
		return animal;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Animal> listAnimals() {
		// haven't tested
		return sessionFactory.getCurrentSession().createQuery("FROM Animal").list();
	}
	
}
