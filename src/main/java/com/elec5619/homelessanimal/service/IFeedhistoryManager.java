package com.elec5619.homelessanimal.service;
import java.io.Serializable;
import java.util.List;
import com.elec5619.homelessanimal.domain.FeedHistory;

public interface IFeedhistoryManager extends Serializable {
	public void addFeedhistory(FeedHistory feedhistory);
//	public FeedHistory getFeedhistoryById(long id);
	public List<FeedHistory> listFeedhistorys(String id);
	public List<FeedHistory> listFeedhistorysByAnimalId(long id);
}
