package com.elec5619.homelessanimal.service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.homelessanimal.domain.User;

@Service(value = "userManager")
@Transactional
public class UserManager implements IUserManager {
	private SessionFactory sessionFactory;

	// private static final Logger logger =
	// LoggerFactory.getLogger(PersonDAOImpl.class);
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addUser(User user) {
		this.sessionFactory.getCurrentSession().save(user);
	}

	public User loginUser(String id, String Password) {
		User user = this.getUserById(id);
		if (user != null && user.getPassword() != null && user.getPassword().equals(Password)) {
			return user;

		}
		return null;
	}

	@Override
	public User getUserById(String id) {
		if (id == null) {
			return getAnonymousUser();
		}
		User user = (User) this.sessionFactory.getCurrentSession().get(User.class, id);
		if (user == null) {
			user = getAnonymousUser();
		}
		return user;
	}

	private User getAnonymousUser() {
		User user = (User) this.sessionFactory.getCurrentSession().get(User.class, "Anonymous User");
		if (user == null) {
			addAnonymousUser();
			user = getAnonymousUser();
		}
		return user;
	}

	private User addAnonymousUser() {
		User user = new User();
		user.setId("Anonymous User");
		addUser(user);
		return user;
	}

	@Override
	public List<User> listUsers() {

		// haven't tested
		return this.sessionFactory.getCurrentSession().createQuery("FROM User").list();

	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub

		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.update(user);
		// logger.info("Person updated successfully, Person Details="+p);

	}

	@Override
	public void deleteUser(String id) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		User user = (User) currentSession.get(User.class, id);
		currentSession.delete(user);
	}

}
