package com.elec5619.homelessanimal.service;

import java.io.Serializable;
import java.util.List;

import com.elec5619.homelessanimal.domain.Animal;

public interface IAnimalManager extends Serializable{
	public void addAnimal(Animal animal);
	public Animal getAnimalById(long id);
	public List<Animal> listAnimals();
}
