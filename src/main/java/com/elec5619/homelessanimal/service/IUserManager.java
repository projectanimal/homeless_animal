package com.elec5619.homelessanimal.service;

import java.io.Serializable;
import java.util.List;

import com.elec5619.homelessanimal.domain.User;

public interface IUserManager extends Serializable{
	public void addUser(User user);
	public User getUserById(String Id);
	public void updateUser(User user);
	public void deleteUser(String Id);
	public List<User> listUsers();
	public User loginUser(String Id, String password);
	
}