package com.elec5619.homelessanimal.service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.homelessanimal.domain.FeedHistory;

@Service(value="feedhistoryManager")
@Transactional
public class FeedHistoryManager implements IFeedhistoryManager{

	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addFeedhistory(FeedHistory feedhistory) {
		this.sessionFactory.getCurrentSession().save(feedhistory);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FeedHistory> listFeedhistorys(String id) {
		return this.sessionFactory.getCurrentSession().createQuery("FROM FeedHistory where user_id='"+id+"'").list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<FeedHistory> listFeedhistorysByAnimalId(long id) {
		return this.sessionFactory.getCurrentSession().createQuery("FROM FeedHistory where animal_id='"+id+"'").list();
	}
}
