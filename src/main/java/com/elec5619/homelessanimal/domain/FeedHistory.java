
package com.elec5619.homelessanimal.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
//import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.util.Date;

@Entity
@Table(name = "Feedhistory")

public class FeedHistory {
	@Id
	@GeneratedValue
	@Column(name = "Id")
	private long id;

	private Date date;
	// private int likeCount;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "animal_id", nullable = false)
	private Animal animal;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", nullable = true)
	private User user;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	// public int getLikeCount() {
	// return likeCount;
	// }
	// public void setLikeCount(int likeCount) {
	// this.likeCount = likeCount;
	// }
	public Animal getAnimal() {
		return animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public boolean equals(FeedHistory feedHistory){
		if (this.id != feedHistory.getId()){
			return false;
		}
		if (!this.date.equals(feedHistory.getDate())){
			return false;
		}
		if (!this.user.equals(feedHistory.getUser())){
			return false;
		}
		if (!this.animal.equals(feedHistory.getAnimal())){
			return false;
		}
		return true;
	}

}
