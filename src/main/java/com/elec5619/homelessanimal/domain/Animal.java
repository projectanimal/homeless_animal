package com.elec5619.homelessanimal.domain;

import java.util.Set;

//import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
//import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
//import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Animal")
public class Animal {
	@Id
	@GeneratedValue
	@Column(name = "Id")
	private long id;

	private double longitude;
	private double latitude;
	private String name;
	private String type;
	private String comment;
	private String photoPath;
	private String sex;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "animal")
	private Set<FeedHistory> feedhistorys;


	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Animal(){};	
	public Animal(double longitude, double latitude, String name, String type, String comment, String photoPath, String sex){
		this.longitude = longitude;
		this.latitude = latitude;
		this.name = name;
		this.type = type;
		this.comment = comment;
		this.photoPath = photoPath;
		this.sex = sex;
	}

	public String toString() {
		String str = "";
		str += this.latitude + "\n";
		str += this.longitude + "\n";
		str += this.name + "\n";
		str += this.type + "\n";
		str += this.comment + "\n";
		str += this.photoPath + "\n";
		return str;
	}	
}
