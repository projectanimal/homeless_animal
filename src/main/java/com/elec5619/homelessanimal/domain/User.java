package com.elec5619.homelessanimal.domain;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="User")
public class User {
	@Id
    private String id;//必须小写
	
    private String password;
    private String email;
//    private Date CreateDate;
//    private boolean Is_admin;
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
	private Set<FeedHistory> feedhistorys;
	
    
	public User(String id, String email){

		this.id = id;
		this.email = email;
	}
       
	public User(){};

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Set<FeedHistory> getFeedhistorys() {
		return feedhistorys;
	}
	public void setFeedhistorys(Set<FeedHistory> feedhistorys) {
		this.feedhistorys = feedhistorys;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String toString() {
		String str = "";
		return str;
	}
}
