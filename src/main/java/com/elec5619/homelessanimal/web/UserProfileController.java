package com.elec5619.homelessanimal.web;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.homelessanimal.domain.FeedHistory;
import com.elec5619.homelessanimal.domain.User;
import com.elec5619.homelessanimal.service.IAnimalManager;
import com.elec5619.homelessanimal.service.IFeedhistoryManager;
import com.elec5619.homelessanimal.service.IUserManager;
//import com.elec5619.homelessanimal.util.ShowFeedhistory;
/*
 * User profile - by Sen
 * */
@Controller
@Scope("session")
public class UserProfileController {

	@Resource(name="userManager")
    	private IUserManager userManager;
	
	@Resource(name="animalManager")
	private IAnimalManager animalManager;
	
	@Resource(name="feedhistoryManager")
	private IFeedhistoryManager feedhistoryManager;
	
//	@Resource(name="animalManager")
//	private IUserManager animalManager;
	
	@RequestMapping(value ="/userhomepage",method = RequestMethod.GET)
	public ModelAndView UserHomePage(HttpSession session)throws FileNotFoundException, IOException{
		//获取数据库信息
		
		User user = userManager.getUserById((String)session.getAttribute("id"));
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("id", (String)session.getAttribute("id"));
		model.put("name", (String)session.getAttribute("id"));
		//model.put("Password", user.getPassword());
		model.put("email", user.getEmail());
		
		List<FeedHistory> feedhistorys=feedhistoryManager.listFeedhistorys((String)session.getAttribute("id"));
		Collections.reverse(feedhistorys);
		

		model.put("feedhistorys", feedhistorys);
		model.put("totalrecord", feedhistorys.size());
		
		return new ModelAndView("UserHomePage", "model", model);
        
       
	}
	
	@RequestMapping(value ="/userhomepage",method = RequestMethod.POST)
	public ModelAndView UserHomePageEdit(HttpSession session)throws FileNotFoundException, IOException{
		//获取数据库信息
		
		User user =  userManager.getUserById((String)session.getAttribute("id"));
		Map<String, Object> model = new HashMap<String, Object>();
		if (session.getAttribute("id")=="Anonymous User"|| session.getAttribute("id")==null  ){
			model.put("id", "Anonymous User");
			model.put("Username", "Anonymous User");
			
		}else{
		model.put("id", (String)session.getAttribute("id"));
		model.put("Username", (String)session.getAttribute("id"));}
		//model.put("Password", user.getPassword());
		model.put("Email", user.getEmail());
		
		
		return new ModelAndView("modifyUserInfo", "model", model);
        
       
	}
	
	@RequestMapping(value ="/modifyuserinfo",method = RequestMethod.GET)

	public ModelAndView ModifyUserPage(HttpSession session)throws FileNotFoundException, IOException{
	User user =  userManager.getUserById((String)session.getAttribute("id"));
	Map<String, Object> model = new HashMap<String, Object>();
	model.put("id", (String)session.getAttribute("id"));
	model.put("name", (String)session.getAttribute("id"));
	model.put("password", user.getPassword());
	model.put("email", user.getEmail());
	
    return new ModelAndView("modifyUserInfo", "model", model);
}
	
	@RequestMapping(value ="/modifyuserinfo",method = RequestMethod.POST)
	public ModelAndView ModifyUser(HttpSession session,@RequestParam("id") String id,@RequestParam("name") String name, @RequestParam("password") String password,
			@RequestParam("email") String email) throws FileNotFoundException, IOException{
		
		/* server side verification */
		if(name.isEmpty()){
			HashMap<String, Object> errorModel = new HashMap<String, Object>();
			errorModel.put("message", "userName can not be empty");
	    	return new ModelAndView("error", "model", errorModel);
	    }
		if(password.isEmpty()){
			HashMap<String, Object> errorModel = new HashMap<String, Object>();
			errorModel.put("message", "Password can not be empty");
	    	return new ModelAndView("error", "model", errorModel);
	    }
		if(email.isEmpty()){
			HashMap<String, Object> errorModel = new HashMap<String, Object>();
			errorModel.put("message", "Email can not be empty");
	    	return new ModelAndView("error", "model", errorModel);
	    }
		
		
		 /* store User profile in database*/
		User user = userManager.getUserById((String)session.getAttribute("id"));
		//do not forget set id or Hibernate error 
		
		session.setAttribute("id",id);
		user.setPassword(password);
		user.setEmail(email);		
		user.setId(id);
		
		userManager.updateUser(user);
		
		
		/* display added user profile */
		Map<String, Object> model = new HashMap<String, Object>();
		
		model.put("id", id);
		model.put("name", user.getId());
//		model.put("password", user.getPassword());
		model.put("email", user.getEmail());
		
		
        
        return new ModelAndView("modifyUserInfo", "model", model);
	}
	

	
}
