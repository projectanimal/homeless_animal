package com.elec5619.homelessanimal.web;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.homelessanimal.domain.Animal;
import com.elec5619.homelessanimal.domain.User;
import com.elec5619.homelessanimal.service.IAnimalManager;
import com.elec5619.homelessanimal.util.Base64Util;

/*
 * Home - by Charles
 * */
@Controller
@Scope("session")
public class HomeController {
	@Resource(name="animalManager")
	private IAnimalManager animalManager;
	
	
	
	@RequestMapping(value={"/", "home"}, method = RequestMethod.GET)
	public ModelAndView indexPage(HttpSession session) throws FileNotFoundException, IOException{
		List<Animal> animals = animalManager.listAnimals();		
		Map<Long, String> photos = new HashMap<Long, String>();
		String photo;
		for(Animal animal : animals){
			photo = Base64Util.toBase64String(new File(animal.getPhotoPath()));
			photos.put(animal.getId(), photo);
		}		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("animals", animals);
		model.put("photos", photos);		
		model.put("id", session.getAttribute("id"));
		return new ModelAndView("index", "model", model);
		
	}
	@RequestMapping(value="/viewinmap", method = RequestMethod.GET)
	public ModelAndView viewSpecificPage(HttpSession session, @RequestParam("lat") String lat, @RequestParam("lon") String lon) throws FileNotFoundException, IOException{
		
		List<Animal> animals = animalManager.listAnimals();		
		Map<Long, String> photos = new HashMap<Long, String>();
		String photo;
		for(Animal animal : animals){
			photo = Base64Util.toBase64String(new File(animal.getPhotoPath()));
			photos.put(animal.getId(), photo);
		}		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("animals", animals);
		model.put("photos", photos);		
		model.put("lat", lat);
		model.put("lon", lon);
		
        return new ModelAndView("index", "model", model);
	}
	
		
	
}
