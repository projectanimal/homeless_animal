package com.elec5619.homelessanimal.web;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.homelessanimal.domain.Animal;
import com.elec5619.homelessanimal.domain.FeedHistory;
import com.elec5619.homelessanimal.domain.User;
//import com.elec5619.homelessanimal.domain.FeedingRecord;
import com.elec5619.homelessanimal.service.IAnimalManager;
import com.elec5619.homelessanimal.service.IFeedhistoryManager;
import com.elec5619.homelessanimal.service.IUserManager;
//import com.elec5619.homelessanimal.service.feed.IRecordManager;
import com.elec5619.homelessanimal.util.Base64Util;

@Controller
@Scope("session")
public class AnimalDetailController {

	@Resource(name = "feedhistoryManager")
	private IFeedhistoryManager feedhistoryManager;

	@Resource(name = "animalManager")
	private IAnimalManager animalManager;

	@Resource(name = "userManager")
	private IUserManager userManager;

	@RequestMapping(value = "/animaldetail", method = RequestMethod.GET)
	public ModelAndView displayAnimalDetail(HttpSession session, @RequestParam("animalid") long animalId) {

		Animal animal = animalManager.getAnimalById(animalId);

		Map<String, Object> model = new HashMap<String, Object>();

		model.put("userid", session.getAttribute("id"));
		model.put("animalid", animalId);
		model.put("name", animal.getName());
		model.put("type", animal.getType());
		model.put("comment", animal.getComment());
		model.put("sex", animal.getSex());
		try {
			model.put("photo", Base64Util.toBase64String(new File(animal.getPhotoPath())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<FeedHistory> feedingHistory = feedhistoryManager.listFeedhistorysByAnimalId(animalId);
		model.put("feedhistorys", feedingHistory);

		return new ModelAndView("animaldetail", "model", model);
	}

	@RequestMapping(value = "/animaldetail", method = RequestMethod.POST)
	public ModelAndView addfeedhistory(HttpSession session, @RequestParam("animalid") long animalId)
			throws IOException {

		Date date = new Date();
		FeedHistory record = new FeedHistory();
		
		if (session.getAttribute("id")=="Anonymous User"|| session.getAttribute("id")==null  ){
			User user = userManager.getUserById("Anonymous User");
			record.setUser(user);
		}
		else{
		User user = userManager.getUserById((String) session.getAttribute("id"));
		record.setUser(user);

		}
		record.setAnimal(animalManager.getAnimalById(animalId));
		record.setDate(date);
		// record.setLikeCount(0);
		feedhistoryManager.addFeedhistory(record);

		//Map<String, Object> model = new HashMap<String, Object>();

		return new ModelAndView("animaldetail");
	}
}
