package com.elec5619.homelessanimal.web;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.elec5619.homelessanimal.domain.Animal;
import com.elec5619.homelessanimal.service.IAnimalManager;
import com.elec5619.homelessanimal.util.Base64Util;
/*
 * Home - by Charles
 * */
@Controller
@Scope("session")
public class ListViewController {

	@Resource(name="animalManager")
	private IAnimalManager animalManager;
	
	@RequestMapping(value ="/listview",method = RequestMethod.GET)
	public ModelAndView listViewPage(@RequestParam("lat") double lat, @RequestParam("lon") double lon) throws FileNotFoundException, IOException{
	
		
		List<Animal> animals = animalManager.listAnimals();		
		Map<Long, String> photos = new HashMap<Long, String>();
		String photo;
		for(Animal animal : animals){
			photo = Base64Util.toBase64String(new File(animal.getPhotoPath()));
			photos.put(animal.getId(), photo);
		}
		
	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("animals", animals);
		model.put("photos", photos);
		model.put("userLat", lat);
		model.put("userLon", lon);
		
		return new ModelAndView("ListView", "model", model);
		
	}
	
	

}
