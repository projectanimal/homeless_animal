package com.elec5619.homelessanimal.web;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.homelessanimal.domain.Animal;
import com.elec5619.homelessanimal.domain.FeedHistory;
import com.elec5619.homelessanimal.service.IAnimalManager;
import com.elec5619.homelessanimal.util.Base64Util;

/*
 * Animal profile - by Sharon
 * */
@Controller
@Scope("session")
public class AnimalProfileController {

	@Resource(name = "animalManager")
	private IAnimalManager animalManager;

	@RequestMapping(value = "/createanimal", method = RequestMethod.GET)
	public ModelAndView createAnimalPage(@RequestParam("lat") String lat, @RequestParam("lon") String lon) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("lat", lat);
		model.put("lon", lon);

		return new ModelAndView("createanimal", "model", model);
	}

	@RequestMapping(value = "/createanimal", method = RequestMethod.POST)
	public ModelAndView createAnimal(@RequestParam("lat") double lat, @RequestParam("lon") double lon,
			@RequestParam("name") String name, @RequestParam("type") String type,
			@RequestParam("comment") String comment, @RequestParam("sex") String sex,
			@RequestParam("photo") MultipartFile multipartFile) throws IOException {

		/* server side verification */
		if (multipartFile.isEmpty()) {
			HashMap<String, Object> errorModel = new HashMap<String, Object>();
			errorModel.put("message", "Photo can not be empty");
			return new ModelAndView("error", "model", errorModel);
		}

		/* create folder if not exist */
		createFolder("photos");

		/* store photo file to relative path */
		String basePath = "photos/";
		String filePath = basePath + UUID.randomUUID() + ".png";
		File file = new File(filePath);
		multipartFile.transferTo(file);

		/* store animal profile in database */
		Animal animal = new Animal();
		animal.setName(name);
		animal.setType(type);
		animal.setLatitude(lat);
		animal.setLongitude(lon);
		animal.setComment(comment);
		animal.setSex(sex);
		animal.setPhotoPath(file.getAbsolutePath()); // store absolute path
		animalManager.addAnimal(animal);

		/* display added animal profile */
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("lat", animal.getLatitude());
		model.put("lon", animal.getLongitude());
		model.put("name", animal.getName());
		model.put("type", animal.getType());
		model.put("comment", animal.getComment());
		model.put("photo", Base64Util.toBase64String(file));
		model.put("sex", animal.getSex());

		return new ModelAndView("displayanimal", "model", model);
	}

	private void createFolder(String path) {
		File theDir = new File(path);

		// if the directory does not exist, create it
		if (!theDir.exists()) {
			boolean result = false;

			try {
				theDir.mkdir();
				result = true;
			} catch (SecurityException se) {
				// handle it
			}
			if (result) {
				System.out.println("DIR created");
			}
		}
	}
}
