package com.elec5619.homelessanimal.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.homelessanimal.util.SendEmailUtil;
import com.elec5619.homelessanimal.domain.User;
import com.elec5619.homelessanimal.service.IUserManager;



/*
 * Signup profile - by Leo
 * */
@Controller
@Scope("session")
public class SignUpController {

	@Resource(name="userManager")
	private IUserManager userManager;
	
	@RequestMapping(value ="/signup",method = RequestMethod.GET)
	public ModelAndView signUpPage(){
		Map<String, Object> model = new HashMap<String, Object>();
		
        return new ModelAndView("signup", "model", model);
	}
	
	@RequestMapping(value ="/login",method = RequestMethod.GET)
	public ModelAndView logInPage(){
		Map<String, Object> model = new HashMap<String, Object>();
		
        return new ModelAndView("login", "model", model);
	}
	
	@RequestMapping(value ="/loginwithfacebook",method = RequestMethod.GET)
	public ModelAndView loginViewFaceBook(HttpSession session, Model model, @RequestParam("token") String token, @RequestParam("id") String id, @RequestParam("name") String name){
		User user = userManager.getUserById(id); 
		if(user == null)
		{
			user = new User();
			user.setId(id);
			userManager.addUser(user);
		}
		
		session.setAttribute("id", user.getId());
		return new ModelAndView("redirect:/home", "model", model);
	}
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public ModelAndView verifyLogin(@RequestParam("id") String id, @RequestParam("password") String password, 
			HttpSession session, Model model) {
		User user = userManager.loginUser(id, password);
		if(user == null){
			model.addAttribute("loginError", "User name and password don't match, please try again!");
			return new ModelAndView("login", "model", model);
		}
		session.setAttribute("id", user.getId());
		return new ModelAndView("redirect:/home", "model", model);
	}
	
//	@RequestMapping(value="/logout", method = RequestMethod.POST)
//	public ModelAndView userstatus(HttpSession session){
//		String status;
//		status = session.getAttribute("id").toString();
//		Map<String, Object> model = new HashMap<String, Object>();
//		if (status == null){
//			model.put("status", "Logout");
//			return new ModelAndView("redirect:/homelessanimal/");
//		}
//		else{
//			return new ModelAndView("redirect:/homelessanimal/login");
//		}
//	}
	 @RequestMapping("/logout")
     public String logout(HttpSession session ) {

         session.removeAttribute("id");
		 session.invalidate();
         return "index";
     }
	 
	@RequestMapping(value ="/signup",method = RequestMethod.POST)
	public ModelAndView signupPostPage(
			@RequestParam("name") String Username, @RequestParam("email") String Email, @RequestParam("password") String Password,HttpSession session
			) throws IOException{
	    
		
		
	    /* store animal profile in database*/
		User user = new User();
		user.setId(Username);
		user.setEmail(Email);
		user.setPassword(Password);
		
		userManager.addUser(user);
		
		/*Send email to user for register successfully*/
		SendEmailUtil send = new SendEmailUtil(user.getEmail(), "Registration success!", "Thank you for register on Feedme!");
		send.sendEmail();
		
		
		/* display added animal profile */
		Map<String, Object> model = new HashMap<String, Object>();
		
		model.put("name", user.getId());
		model.put("email", user.getEmail());
		model.put("password", user.getPassword());
		
		session.setAttribute("id", user.getId());
		return new ModelAndView("index", "model", model);
	}

}
