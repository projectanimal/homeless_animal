package com.elec5619.homelessanimal.service;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.elec5619.homelessanimal.domain.Animal;
import com.elec5619.homelessanimal.service.IAnimalManager;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml","file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml"})
public class AnimalServiceTest {
	
	@Resource(name="animalManager")
	private IAnimalManager animalManager;
	
	@Test
    public void testAddAnimal(){	
		Animal animal = new Animal();
		animal.setLatitude(123.321);
		animal.setLongitude(123.321);
		animal.setName("puppyyyy");
		animal.setType("Dog");
		animal.setComment("here is a puppyyyyy");
		animalManager.addAnimal(animal);
	}
	
	@Test
    public void testListAnimal(){
		// including the one we just added above
//		Assert.assertEquals(6, animalManager.listAnimals().size());
	}
	
	@Test
    public void testGetAnimalById(){
//		Assert.assertEquals(1, animalManager.getAnimalById(1).getId());
	}
}
