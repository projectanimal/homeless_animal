package com.elec5619.homelessanimal.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.homelessanimal.domain.Animal;
import com.elec5619.homelessanimal.domain.FeedHistory;
import com.elec5619.homelessanimal.domain.User;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml",
		"file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml" })
@Transactional
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class FeedHistoryManagerTest {

	@Resource(name = "feedhistoryManager")
	private FeedHistoryManager feedHistoryManager;

	@Resource(name = "animalManager")
	private IAnimalManager animalManager;

	@Resource(name = "userManager")
	private IUserManager userManager;

	private List<FeedHistory> feedHistoryList;
	FeedHistory fh1, fh2;
	private Date date1, date2;
	User user1, user2;
	Animal animal1, animal2;

	@Before
	public void setUp() throws Exception {
		feedHistoryList = new ArrayList<FeedHistory>();

		fh1 = new FeedHistory();
		fh2 = new FeedHistory();
		user1 = new User();
		user2 = new User();
		animal1 = new Animal();
		animal2 = new Animal();

		user1.setId("user1");
		user1.setEmail("user1@test.com");
		user1.setPassword("user1");

		user2.setId("user2");
		user2.setEmail("user2@test.com");
		user2.setPassword("user2");

//		 execute only one time.
		 userManager.addUser(user1);
		 userManager.addUser(user2);

		animal1.setId(10000);
		animal1.setLongitude(1.0);
		animal1.setLatitude(1.0);
		animal1.setName("animal1");
		animal1.setPhotoPath("/animal1");
		animal1.setSex("male");
		animal1.setType("dog");
		animal1.setComment("animal1");

		animal2.setId(10001);
		animal2.setLongitude(2.0);
		animal2.setLatitude(2.0);
		animal2.setName("animal2");
		animal2.setPhotoPath("/animal2");
		animal2.setSex("female");
		animal2.setType("cat");
		animal2.setComment("animal2");

		// execute only one time.
		 animalManager.addAnimal(animal1);
		 animalManager.addAnimal(animal2);

		date1 = new Date();
		date2 = new Date();
		fh1.setDate(date1);
		fh1.setId(10001);
		fh1.setUser(user1);
		fh1.setAnimal(animal1);
		fh2.setDate(date2);
		fh2.setId(10002);
		fh2.setUser(user2);
		fh2.setAnimal(animal2);
		fh2.setDate(date2);

		feedHistoryList.add(fh1);
		feedHistoryList.add(fh2);

		// execute only one time.
		feedHistoryManager.addFeedhistory(fh1);
		feedHistoryManager.addFeedhistory(fh1);
	}

	@Test
	public void testListFeedHistory() {
		List<FeedHistory> fhl = feedHistoryManager.listFeedhistorys("user1");
		for (FeedHistory fh : fhl) {
			Assert.assertTrue(fh.equals(fh1));
		}
	}

	@Test
	public void testListFeedHistoryByAnimalId() {
		List<FeedHistory> fhl = feedHistoryManager.listFeedhistorysByAnimalId(10002);
		for (FeedHistory fh : fhl) {
			Assert.assertTrue(fh.equals(fh2));
		}
	}
}
