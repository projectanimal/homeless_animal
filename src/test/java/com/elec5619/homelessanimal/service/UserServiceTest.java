package com.elec5619.homelessanimal.service;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import com.elec5619.homelessanimal.domain.User;
import com.elec5619.homelessanimal.service.IUserManager;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml","file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml"})
public class UserServiceTest {
	@Resource(name="userManager")
	private IUserManager userManager;
	
	@Test
    public void testAddUser(){	
		User user = new User();
		user.setId("Wen");
		user.setEmail("chensen14@126.com");
		user.setPassword("123");
		userManager.addUser(user);
	}
	
	@Test
    public void testListUser(){
		// including the one we just added above
		for(User user:userManager.listUsers() ){
			System.out.println(user.getEmail());
		}		
	}
	
	@Test
    public void deleteUser(){		
		userManager.deleteUser("Sen");
	}
	
	@Test
    public void testGetUserById(){		
		System.out.println(userManager.getUserById("Sen").getEmail());
	}
}
