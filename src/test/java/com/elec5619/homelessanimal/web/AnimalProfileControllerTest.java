package com.elec5619.homelessanimal.web;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.server.MockMvc;
import org.springframework.test.web.server.setup.MockMvcBuilders;

import com.elec5619.homelessanimal.util.Base64Util;

import static org.springframework.test.web.server.request.MockMvcRequestBuilders.get; 
import static org.springframework.test.web.server.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.*;


@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml","file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml"})
public class AnimalProfileControllerTest{
	
	private MockMvc mockMvc;

    @Before
    public void setup() 
    {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
        		.xmlConfigSetup("file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml","file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml")
        		.build();
    }
	
	@Test
	public void testGetCreateAnimalPage() throws Exception{
		String lat = "-321.21";
		String lon = "158.22";
		
		// expected response model
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("lat", lat);
		model.put("lon", lon);
		
		// send request
		mockMvc.perform(get("/createanimal?lat="+lat+"&lon="+lon))
			.andExpect(status().isOk())
			.andExpect(view().name("createanimal"))
			.andExpect(model().attribute("model", model));
	}

	@Test
	public void testPostCreateAnimal() throws Exception{
		// request data
		double lat = -32.22;
		double lon = 150.88;
		String name = "puppy";
		String type = "Dog";
		String comment = "here is a dog";
		String sex = "female";
		byte[] photo = "this is an empty image".getBytes();
		MockMultipartFile firstFile = new MockMultipartFile("photo", "testphoto.png", "image/*", photo);
		
		// expected response model
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("lat", lat);
		model.put("lon", lon);
		model.put("name", name);
		model.put("type", type);
		model.put("comment", comment);
		model.put("sex", sex);
		model.put("photo", Base64Util.toBase64String(photo));
		
		// send request
		mockMvc.perform(fileUpload("/createanimal")
                .file(firstFile)
                .param("lat", Double.toString(lat))
                .param("lon", Double.toString(lon))
                .param("name", name)
                .param("type", type)
                .param("comment", comment)
                .param("sex", sex)).andExpect(status().isOk())
				.andExpect(view().name("displayanimal"))
				.andExpect(model().attribute("model", model));
	}
}