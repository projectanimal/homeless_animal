package com.elec5619.homelessanimal.web;

import javax.servlet.http.HttpSession;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.server.MockMvc;
import org.springframework.test.web.server.setup.MockMvcBuilders;

import com.elec5619.homelessanimal.util.Base64Util;

import static org.springframework.test.web.server.request.MockMvcRequestBuilders.get; 
import static org.springframework.test.web.server.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.server.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.*;


import junit.framework.TestCase;

import org.springframework.web.servlet.ModelAndView;
@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml","file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml"})
public class UserProfileControllerTest extends TestCase{
	private MockMvc mockMvc;

    @Before
    public void setup() 
    {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
        		.xmlConfigSetup("file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml","file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml")
        		.build();
    }
    
	@Test
	public void testUserHomePageView() throws Exception{
		UserProfileController cont= new UserProfileController();
		Map<String, Object> model = new HashMap<String, Object>();
		
//		HttpSession sess=new MockHttpSession();		
//		sess.setAttribute("id", "Sen");
		
		mockMvc.perform(get("/userhomepage")).andExpect(status().isOk()).andExpect(view().name("userhomepage"))
		.andExpect(model().attribute("model", model));
		
		

	}
	
	@Test
	public void testUserHomePageEditView() throws Exception{
		UserProfileController cont= new UserProfileController();
		Map<String, Object> model = new HashMap<String, Object>();
		// request data
	
		String id = "Sen";
		String name = "Sen";
		String email = "chensen14@126.com";
		
		model.put("id", id);
		model.put("Username", name);
		model.put("Email", email);
		
		mockMvc.perform(fileUpload("/userhomepage")
				.param("id", id)
				.param("Username", name)
				.param("Email",email))
				.andExpect(status().isOk())
				.andExpect(view().name("modifyUserInfo"))
				.andExpect(model().attribute("model", model));

	}
	
	@Test
	public void testModifyUserPageView() throws Exception{
		UserProfileController cont= new UserProfileController();
		Map<String, Object> model = new HashMap<String, Object>();
		
//		HttpSession sess=new MockHttpSession();		
//		sess.setAttribute("id", "Sen");
		
		mockMvc.perform(get("/modifyuserinfo")).andExpect(status().isOk()).andExpect(view().name("modifyuserinfo"))
		.andExpect(model().attribute("model", model));
		
		

	}
	@Test
	public void testModifyUserView() throws Exception{
		UserProfileController cont= new UserProfileController();
		Map<String, Object> model = new HashMap<String, Object>();
		// request data
	
		String id = "Sen";
		String name = "Sen";
		String email = "chensen14@126.com";
		
		model.put("id", id);
		model.put("Username", name);
		model.put("Email", email);
		
		mockMvc.perform(fileUpload("/modifyuserinfo")
				.param("id", id)
				.param("name", name)
				.param("email",email))
				.andExpect(status().isOk())
				.andExpect(view().name("modifyUserInfo"))
				.andExpect(model().attribute("model", model));

	}
	
	
}
