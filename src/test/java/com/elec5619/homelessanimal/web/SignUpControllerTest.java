package com.elec5619.homelessanimal.web;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.server.MockMvc;
import org.springframework.test.web.server.setup.MockMvcBuilders;

import static org.springframework.test.web.server.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.*;


@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml","file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml"})
public class SignUpControllerTest{
	
	private MockMvc mockMvc;

    @Before
    public void setup() 
    {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders
        		.xmlConfigSetup("file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml","file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml")
        		.build();
    }


	@Test
	public void testSignUp() throws Exception{
		// request data

		String name = "Leon";
		String email = "lniu0297@gmail.com";
		String password = "taozitaozi";
		
		// expected response model
		Map<String, Object> model = new HashMap<String, Object>();
		
		model.put("name", name);
		model.put("email", email);
		model.put("password", password);
		
		
		// send request
		mockMvc.perform(
				fileUpload("/signup") 
                .param("name", name)
                .param("email", email)
                .param("password", password))
				.andExpect(status().isOk())
				.andExpect(view().name("index"))
				.andExpect(model().attribute("model", model));
	}
}