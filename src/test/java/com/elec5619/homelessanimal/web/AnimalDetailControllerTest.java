package com.elec5619.homelessanimal.web;

import static org.springframework.test.web.server.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.view;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.web.server.MockMvc;
import org.springframework.test.web.server.MvcResult;
import org.springframework.test.web.server.ResultHandler;
import org.springframework.test.web.server.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.homelessanimal.domain.User;
import com.elec5619.homelessanimal.service.IUserManager;

import static org.springframework.test.web.server.request.MockMvcRequestBuilders.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml",
		"file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml" })
// @Transactional
// @TransactionConfiguration(transactionManager = "transactionManager",
// defaultRollback = true)
public class AnimalDetailControllerTest {

	private MockMvc mockMvc;
	private SessionHolder sessionHolder;
	@Resource(name = "userManager")
	private IUserManager userManager;

	User testUser;

	@Before
	public void setup() {

		testUser = new User();
		testUser = userManager.getUserById("test user");
		if (testUser.getId().equals("Anonymous User")) {
			testUser.setId("test user");
			testUser.setEmail("test@test.com");
			testUser.setPassword("test user");
			userManager.addUser(testUser);
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		sessionHolder = new SessionHolder();
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders
				.xmlConfigSetup("file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml",
						"file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml")
				.build();
	}

	@Test
	public void testGetAnimalDetailPage() throws Exception {
		// String lat = "-321.21";
		// String lon = "158.22";
		//
		// // expected response model
		// Map<String, Object> model = new HashMap<String, Object>();
		// model.put("lat", lat);
		// model.put("lon", lon);
		//
		// // send request
		// mockMvc.perform(get("/createanimal?lat="+lat+"&lon="+lon))
		// .andExpect(status().isOk())
		// .andExpect(view().name("createanimal"))
		// .andExpect(model().attribute("model", model));

		try {
			this.mockMvc.perform(post("/login").param("id", "test user").param("password", "test user"))
					.andExpect(status().isOk()).andDo(new ResultHandler() {
						@Override
						public void handle(MvcResult result) throws Exception {
							sessionHolder.setSession(new SessionWrapper(result.getRequest().getSession()));
							if (result.getRequest().getSession() == null) {
								System.out.println(sessionHolder.getSession().getAttribute("null session"));
							}
						}
					});
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println(sessionHolder.getSession().getAttribute("id"));
	}

	private static final class SessionHolder {
		private SessionWrapper session;

		public SessionWrapper getSession() {
			return session;
		}

		public void setSession(SessionWrapper session) {
			this.session = session;
		}
	}

	private static class SessionWrapper extends MockHttpSession {
		private final HttpSession httpSession;

		public SessionWrapper(HttpSession httpSession) {
			this.httpSession = httpSession;
		}

		@Override
		public Object getAttribute(String name) {
			return this.httpSession.getAttribute(name);
		}

	}
}
