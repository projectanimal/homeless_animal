package com.elec5619.homelessanimal.domain;


import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;

public class AnimalTest extends TestCase {

    private Animal animal;

    protected void setUp() throws Exception {
        animal = new Animal();
    }
    
    public void testSetAndGetLongitude(){
    	double testLongitude = -33.333333333;
    	assertEquals(0.0, animal.getLongitude(), 0);    	
    	animal.setLongitude(testLongitude);
    	assertEquals(testLongitude,animal.getLongitude(), 0);
    }
    
    public void testSetAndGetLatitude(){
    	double testLatitude = -33.333333333;
    	assertEquals(0.0, animal.getLatitude(), 0);    	
    	animal.setLatitude(testLatitude);
    	assertEquals(testLatitude,animal.getLatitude(), 0);
    }
       
    public void testSetAndGetName(){
    	String testName = "puppy";
    	assertNull(animal.getName());
    	animal.setName(testName);
    	assertEquals(testName,animal.getName());
    }
    
    public void testSetAndGetType(){
    	String testType = "dog";
    	assertNull(animal.getType());
    	animal.setType(testType);
    	assertEquals(testType,animal.getType());
    }
    
    public void testSetAndGetComment(){
    	String testComment = "This is a cut dog";
    	assertNull(animal.getComment());
    	animal.setComment(testComment);
    	assertEquals(testComment,animal.getComment());
    }
    
    public void testSetAndGetPhotoPath(){
    	String testPhotoPath = "http://www.akjxzhckjh.com";
    	assertNull(animal.getPhotoPath());
    	animal.setPhotoPath(testPhotoPath);
    	assertEquals(testPhotoPath,animal.getPhotoPath());
    }

}