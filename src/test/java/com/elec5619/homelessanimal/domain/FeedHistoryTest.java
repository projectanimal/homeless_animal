package com.elec5619.homelessanimal.domain;

import java.util.Date;

import junit.framework.TestCase;

public class FeedHistoryTest extends TestCase{
	private FeedHistory feedHistory;
	
	protected void setUp(){
		feedHistory = new FeedHistory();
	}
	
	public void testSetAndGetUser() {
        User user;
        assertNull(feedHistory.getUser());
        
        user = new User();
        user.setId("test id");
        user.setEmail("test@test.com");
        user.setPassword("test");
        feedHistory.setUser(user);
 
        assertTrue(user.equals(feedHistory.getUser()));
    }
	
	public void testSetAndGetAnimal() {
        Animal animal;
        assertNull(feedHistory.getAnimal());
        
        animal = new Animal();
        animal.setId(1);
        animal.setLatitude(1.0);
        animal.setLongitude(1.0);
        animal.setName("test");
        animal.setComment("test");
        animal.setPhotoPath("/test");
        animal.setSex("male");
        animal.setType("dog");
        feedHistory.setAnimal(animal);
 
        assertTrue(animal.equals(feedHistory.getAnimal()));
    }
	
	public void testSetAndGetId() {
		assertEquals(0,feedHistory.getId());
 
        feedHistory.setId(10000);
 
        assertEquals(10000,feedHistory.getId());
    }
	
	public void testSetAndGetDate() {
        assertNull(feedHistory.getDate());
        
        Date date = new Date();
        feedHistory.setDate(date);
 
        assertEquals(date,feedHistory.getDate());
    }
}
