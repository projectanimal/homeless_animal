package com.elec5619.homelessanimal.domain;

import junit.framework.TestCase;

public class UserTest extends TestCase{
	
	private User user;
	
	protected void setUp() throws Exception {
        user = new User();
    }
    
    public void testSetAndGetPassword(){
    	String testPassword = "123";
    	assertNull(user.getPassword());  	
    	user.setPassword(testPassword);
    	assertEquals(testPassword,user.getPassword());
    }
    
    public void testSetAndGeteEmail(){
    	String testEmail = "123878@11.com";
    	assertNull(user.getEmail());  	
    	user.setEmail(testEmail);
    	assertEquals(testEmail,user.getEmail());
    }

}
