# README #

Database configuration #

In its default configuration, Feedme uses MySql as a persistent database configuration. 
For MySQL database, it is needed to run with 'MySQL' profile defined in database.properties file.

* jdbc.databaseName=homelessanimal
* jdbc.url=jdbc:mysql://localhost:3306/homelessanimal?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull
* jdbc.username=root
* jdbc.password=root


# Working with Feedme in STS #

## Prerequisites ##

The following items should be installed in your system:

* Maven 4
* JRE System Library [Java SE 8 [1.8.0_40]]
* git command line tool
* STS (Version: 3.7.3.RELEASE)
* Tomcat v8.0

## Steps ##

1) In the command line

* git clone https://lniu0297@bitbucket.org/projectanimal/homeless_animal.git

2) Inside STS

* File -> Import -> Maven -> Existing Maven project

3) Select Tomcat Server and Run

* Run -> Run

4) Select Tomcat Server and Run

* Open browser and input the url: localhost:8080/homelessanimal/login
Turn to login page and begin to use this website.




### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact